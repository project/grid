<?php

/* 	This example shows how the grid module can be used to create forms with repeating rows.
	The example code is taken from a working module and you should be able to adapt it to suit your needs.  Here is the procedure in outline.
	
	Defining your form
	==================
	The procedure when defining forms is to define the main part of the form and one (or more) subforms.
	grid_merge is then used to combine the forms into repeating rows
	
	Getting data from database
	==========================
	One query, which returns a single row, is needed to get data for the main form.
	A second query, which returns more than one row, is needed to get data for the repeating rows.  This data is placed in a 2-dimensional array [row][column] which becomes part of the mode being edited
	
	Validating data
	===============
	grid_unmerge can be used to convert the form into a 2-dimensional array

	You can iterate through the rows to validate each row in turn.  While doing so, you should check that the row is non-blank before doing any validation.
	The Drupal function grid_set_error can be used to mark any columns that are in error, eg missing fields etc.  The general usage is:
		grid_set_error($subform, $row, $col, 'Error message')
		where:
			$subform is the title of the sub form
			$row is the row where the ewrror occurred
			$col is the column where the error occurred
	
	Inserting or updating into the database
	=======================================
	grid_unmerge converts the form data into a 2-dimensional array - removing blank rows in the process
	
	The example
	===========
	The following example uses standard Drupal hooks for a module called m360_framework.  Some parts, shown by ...,  have been stripped out to aid clarity.  Comments have beed added to show how the grid functionality is implemented.
*/

/*	Define the form
	===============
*/
function m360_framework_form(&$node) {
	
	// main parts of form
	$form = array(
		'title' => array(
			'#type' => 'textfield',
			'#title' => t('Title'),
			'#description' => t('Title to identify this framework'),
			'#required' => TRUE,
			'#default_value' => $node->title,
			'#weight' => 1
		),
		'description' => array(
			'#type' => 'textarea',
			'#title' => t('Description'),
			'#description' => t('Further information about this framework'),
			'#required' => FALSE,
			'#default_value' => $node->description,
			'#weight' => 2
		),
		'importance_scale' => ... 
		),
		'performance_scale' => ... 
		),
		// the main form also needs to contain an empty fieldset, which is where the repeating rows from the subform will be placed
		'framework' => array(
			'#type' => 'fieldset',
			'#title' => t('Framework'),
			'#description' => t('The elements of the framework'),
			'#expanded' => TRUE,
			'#weight' => 6
		)
	);
	
	// a second form is defined which defines the repeating elements, ie the columns of the repeating rows
	$frameform = array(
		'id' => array(
			'#type' => 'textfield',
			'#title' => 'Identifier',
			'#description' => t('Used to identify this item independently of the sequence of the items'),
			'#weight' => 1
		),
		'level' => array(
			'#type' => 'select',
			'#title' => 'Level',
			'#description' => t('Position of this item in the hierarchical structure - each item is automatically associated with its parent'),
			'#options' => array(
				''=>'',
				1 => 1,
				2 => 2,
				3 => 3,
				4 => 4,
				5 => 5
			),
			'#weight' => 2
		),
		'type' => array(
			'#type' => 'select',
			'#title' => 'Type',
			'#description' => t('Type of item - Importance, Performance and Remark will place the chosen element in the questionnaire'),
			'#options' => array(
				'' => '',
				'Cluster' => 'Cluster',
				'Importance' => 'Importance',
				'Performance' => 'Performance',
				'Remark' => 'Remark',
			),
			'#weight' => 3
		),
		'description' => array(
			'#type' => 'textfield',
			'#title' => 'Description',
			'#description' => t('Description of this option seen by the user'),
			'#weight' => 4
		),
		'longdescription' => array(
			'#type' => 'textarea',
			'#title' => 'Long description',
			'#description' => t('Additional long description, used to give a more precise definition of the item'),
			'#rows' => 2,
			'#weight' => 5
		),
	);
	
	// now we merge the two forms together
	grid_merge($form, 'framework', $frameform, 30, 10, $node->framework);
		/* 	$form is the main form
			'framework' is the name of the fieldset which will contain the subform
			$frameform is the definition of the subform
			30 is the minimum number of rows in the form
			10 is the number of spare blank rows for new data
			$node->framework is the element of the node being edited which contains a 2-dimensional array of rows and columns, which will be placed in the form
		*/
	
	// finally return the merged form
	return $form;
	
}


/*	=======================
	Load data from database
	-----------------------
*/
function m360_framework_load($node) {
	// the first query returns data for the main form and places it into an object
	$ret = db_fetch_object(db_query("SELECT description, importance_scale, performance_scale FROM {m360_framework} WHERE nid=%d", $node->nid));
	
	// the second query returns repeating data, destined for the subform.  The result is iterated to put the data into a 2-dimensional array, $framework[row][column]
	$res = db_query("SELECT rid, id, level, type, description, longdescription FROM {m360_framework_item} WHERE nid=%d", $node->nid);
	$framework = array();
	while ($row = db_fetch_array($res)) {
		$rid = $row['rid'];
		$id = $row['id'];
		$level = $row['level'];
		$type = $row['type'];
		$description = $row['description'];
		$longdescription = $row['longdescription'];
		$framework[$rid][0] = $id;
		$framework[$rid][1] = $level;
		$framework[$rid][2] = $type;
		$framework[$rid][3] = $description;
		$framework[$rid][4] = $longdescription;
	}
	
	// the 2-dimensional array of repeating data is inserted into a property of the main object
	$ret->framework = $framework;
	
	// finally the object is returned
	return $ret; 
}

/*	===============
	Validate record
	---------------
*/
function m360_framework_validate($node, &$form) {
	// grid_unmerge is called to convert the data returned from the form into a 2-dimensional array, which is easier to process
	// Effectively, this will take the data in the 'framework' fieldset and place it in $node->framework
	// node the 3rd parameter is set to false in order to preserve row numbers
	grid_unmerge($node, 'framework', false);
	
	// iterate through each row of the form to perform validations - blank rows have already been removed from the array
	foreach ($node->framework as $rid=>$row) {
		// extract data into variables (just for clarity)
		$id = $row[0]
		$level = $row[1]
		$type = $row[2]
		$description = $row[3]
		$long_description = $row[4]
		// mandatory fields - note how grid_set_error is used to mark invalid fields
		if ($id == '') {grid_set_error('framework', $rid, 0, 'Identifier must be specified on highlighted row(s)');}
		if ($level == '') {grid_set_error('framework', $rid, 1, 'Level must be specified on highlighted row(s)');}
		if ($description == '') {grid_set_error('framework', $rid, 3, 'Description must be specified on highlighted row(s)');}
		
	}
}

/*	===================
	Insert a new record
	-------------------
*/
function m360_framework_insert(&$node) {
	// use grid_unmerge again to convert the form data into a 2-dimensional array
	// note the 3rd parameter is not set - this will default to true and will renumber the rows sequentially
	grid_unmerge($node, 'framework');
	
	// insert the header
	db_query("INSERT INTO {m360_framework} (nid, title, description, importance_scale, performance_scale) VALUES (%d, '%s', '%s', %d, %d)", $node->nid, $node->title, $node->description, $node->importance_scale, $node->performance_scale);
	// update each row
	foreach ($node->framework as $key=>$value) {
		// insert row
		db_query("INSERT INTO {m360_framework_item} (nid, rid, id, level, type, description, longdescription) VALUES (%d, %d, '%s', %d, '%s', '%s', '%s' )", $node->nid, $key, $value[0], $value[1], $value[2], $value[3], $value[4]);
	}
}

/*	=============
	Save a record
	-------------
	All logic handled by delete and insert operations
*/
function m360_framework_update(&$node) {
	m360_framework_delete($node);
	m360_framework_insert($node);
}

/*	===============
	Delete a record
	---------------
*/
function m360_framework_delete($node) {
	db_query("DELETE FROM {m360_framework_item} WHERE nid=%d", $node->nid);
	db_query("DELETE FROM {m360_framework} WHERE nid=%d", $node->nid);
}

